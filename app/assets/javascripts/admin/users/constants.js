export const USER_AVATAR_SIZE = 32;

export const SHORT_DATE_FORMAT = 'd mmm, yyyy';

export const LENGTH_OF_USER_NOTE_TOOLTIP = 100;
